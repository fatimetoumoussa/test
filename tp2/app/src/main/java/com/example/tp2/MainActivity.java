package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText input1, input2;
    private TextView resultat, er;
    private Button calcule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setlistners();
    }

    private void init() {

        input1 = findViewById(R.id.input1);
        input2 = findViewById(R.id.input2);
        er = findViewById(R.id.er);
        resultat = findViewById(R.id.result);
        calcule = findViewById(R.id.calcule);
    }

    private void setlistners() {
        calcule.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (input1.getText().toString().isEmpty() || input2.getText().toString().isEmpty()) {
            er.setText("entrer des nombres dans les deux champs");


        } else {
            er.setText("");
            int somme = Integer.parseInt(input1.getText().toString()) + Integer.parseInt(input2.getText().toString());
            resultat.setText(Integer.toString(somme));

            Intent  inte  = new Intent(getApplicationContext(), MainActivity2.class);
            inte.putExtra("a", input1.getText().toString());
            inte.putExtra("b", input2.getText().toString());
            startActivity(inte);
        }



    }
}