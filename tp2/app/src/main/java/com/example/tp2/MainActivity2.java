package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    private TextView num1,num2,somme;
    private Button but;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        num1=findViewById(R.id.num1);
        num2=findViewById(R.id.num2);
        somme=findViewById(R.id.somme);
        but=findViewById(R.id.retourner);

        String n1=getIntent().getExtras().getString("a");
        String n2=getIntent().getExtras().getString("b");
        num1.setText(n1);
        num2.setText(n2);
       int s=Integer.parseInt(n1)+Integer.parseInt(n2);
        somme.setText(Integer.toString(s));
       but.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
        Intent i=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(i);
      }
      });


    }
}